package main

import (
	"bufio"
	"fmt"
	"github.com/axgle/mahonia"
	"io/ioutil"
	"os"
	"path"
	"strings"
	"sync"
)

var(
	DirThreads= sync.WaitGroup{}
	PurposeLine=3 // 指定读取第三行
)

func init() {
	DirThreads.Add(1)
}

func getSuffix(absFilePath string)string{
	return path.Ext(absFilePath)
}

func GetAllFileName(path string)error{
	defer DirThreads.Done()

	needPath:=path+"/"
	readDir,err:=ioutil.ReadDir(needPath)
	if err != nil{
		return err
	}

	for _,item:=range readDir {
		if item.IsDir(){
			continue
		}else{
			fullPath:=needPath+item.Name()
			if getSuffix(fullPath)==".txt"{
				// 每个文件都进行一次累计
				DirThreads.Add(1)
				changeNovelFileName(fullPath,needPath)
			}
		}
	}

	return nil
}

func changeNovelFileName(fileName string,path string)error{
	defer DirThreads.Done()

	theFile,err:=os.Open(fileName)
	defer theFile.Close()
	if err != nil{
		return err
	}

	theDecoder:= mahonia.NewDecoder("gbk")
	purposeFile:= theDecoder.NewReader(theFile)

	reader:=bufio.NewScanner(purposeFile)

	idx:=0
	for reader.Scan() {
		if idx==PurposeLine {
			// 获得名字
			newFileName:=reader.Text()
			theFile.Close()
			myFileName:=strings.Trim(newFileName," ")
			fmt.Println(myFileName)
			os.Rename(fileName,path+myFileName+".txt")
			break
		}

		idx++
	}

	return nil
}
